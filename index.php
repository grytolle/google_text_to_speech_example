<?php
require_once('Outputter.class.php');
require_once('TextToSpeech.class.php');
require_once('CreateZip.class.php');

$outputter = new Outputter('<li>', '</li>');

if (isset($_POST['submitText'])) {

    $texts = preg_split('/[\r\n]+|\r+|\n+/', $_POST['texts']);

    if (!isset($_POST['onePerLine'])) {
        $texts = [implode("\n", $texts)];
    }

    $apiKey = $_POST['apiKey'];

    $speaker = new TextToSpeech($apiKey, [$outputter, 'writeLine']);

    list($success, $results) = $speaker->createTexts($texts);

    if ($success) {
        CreateZip::createAndSend($outputter, $results);
    }
    else {
        $outputter->writeLine($results);
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Google Text to Speech</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <script>
        $(function() { //set name = id for all inputs and restore form data
            var formData = <?php echo json_encode((object)[
                'apiKey' => $_POST['apiKey'] ?? '',
                'texts' => $_POST['texts'] ?? '',
                'onePerLine' => isset($_POST['onePerLine']),
            ]); ?>;

            $('form').find(':input').each(function() {
                var el = $(this);
                var id = el.attr('id');
                el.attr('name', id);

                if (el.attr('type') == 'checkbox') {
                    el.prop('checked', formData[id] || false);
                }
                else {
                    el.val(formData[id] || '');
                }
            });
        });
    </script>
</head>

<body>
<div style="margin: 20px; max-width: 800px;">
    <h1>Google Text to Speech</h1>

    <ul id="output"><?php echo $outputter->getText(); ?></ul>

    <form method="post">
        <div class="form-group">
            <label for="apiKey">API key</label>
            <input type="text" class="form-control" id="apiKey" placeholder="API key">
        </div>
        <div class="form-group">
            <label for="texts">Text</label>
            <textarea class="form-control" id="texts" rows="10  "></textarea>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="onePerLine" value="true">
            <label class="form-check-label" for="onePerLine">One audio file per line</label>
        </div>
        <br/>
        <div class="text-right mb-3">
        <button id="submitText" type="submit" class="btn btn-primary">Download files</button>
        </div>
    </form>
</div>
</body>

</html>