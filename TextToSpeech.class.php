<?php

class TextToSpeech {
    private $key = null;
    private $settings = '{
  "audioConfig": {
    "audioEncoding": "MP3",
    "pitch": 0,
    "speakingRate": 1
  },
  "input": {
    "text": ""
  },
  "voice": {
    "languageCode": "en-US",
    "name": "en-US-Wavenet-D"
  }
}';
    private $writeLineFunc = null;

    function __construct($key, $writeLineFunc = null)
    {
        $this->key = $key;

        if ($writeLineFunc == null) {
            $this->writeLineFunc = function($s) {
                echo $s;
            };
        }
        else {
            $this->writeLineFunc = $writeLineFunc;
        }
    }

    private function writeLine($s) {
        $func = $this->writeLineFunc;
        $func($s);
    }

    function createTexts($texts)
    {
        $result = [];

        for ($x = 0; $x < count($texts); $x++) {
            $text = $texts[$x];

            list($success, $file) = $this->createFile($x, $text);

            if (!$success) {
                return [false, $file->message];
            }

            $result[] = (object)[
                'number' => $x,
                'raw'    => $file,
                'text'   => $text,
            ];
        }

        return [true, $result];
    }


    function createFile($number, $text)
    {
        $baseUrl = "https://texttospeech.googleapis.com/v1beta1/text:synthesize";
        $completeUrl = $baseUrl . '?key=' . $this->key;

        $json = $this->settings;

        $data = json_decode($json);
        $data->input->text = $text;

        $options = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'ignore_errors' => true,
                'content' => json_encode($data),
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($completeUrl, false, $context);

        $jsonResponse = json_decode($result);

        if (isset($jsonResponse->error)) {
            return [false, $jsonResponse->error];
        }
        else {
            $decoded = base64_decode($jsonResponse->audioContent);
            return [true, $decoded];
        }
    }
}