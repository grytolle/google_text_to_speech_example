<?php
class CreateZip {
    public static function createAndSend($outputter, $results)
    {
        $zip = new ZipArchive();

        $zipPath = tempnam(sys_get_temp_dir(), 'speech.');
        unlink($zipPath);
        $zipPath .= '.zip';

        if ($zip->open($zipPath, ZipArchive::CREATE) !== TRUE) {
            $outputter->writeLine('Failed to create zip file');
        } else {

            foreach ($results as $result) {
                if ($result->raw != null) {
                    $fileName = str_pad($result->number, 3, '0', STR_PAD_LEFT);
                    $zip->addFromString($fileName . '.mp3', $result->raw);
                    $zip->addFromString($fileName . '.txt', $result->text);
                    $outputter->writeLine('Saved in zip with ID' . $fileName);
                }
            }
            $zip->close();

            header("Content-Type: application/zip");
            header("Content-Disposition: attachment; filename=" . basename($zipPath));
            header("Content-Length: " . filesize($zipPath));

            readfile($zipPath);
            unlink($zipPath);
            exit;
        }
    }
}