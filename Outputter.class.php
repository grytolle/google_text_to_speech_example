<?php

class Outputter {

    private $before = '';
    private $text = '';
    private $after = '';

    public function __construct($before = '', $after = '')
    {
        $this->before = $before;
        $this->after = $after;
    }

    public function writeLine($s) {
        $this->text .= $this->before.$s.$this->after;
    }

    public function getText() {
        return $this->text;
    }
}